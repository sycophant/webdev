<!-- 057a79310b77f82788209eae38715fcf
api.openweathermap.org/data/2.5/weather?zip={zip code} -->

# OpenWeatherAPI-Assignment
Starter code for the Open WeatherMap AJAX assignment.  Original code illustrates the process by making a call to a fake API at zyBooks.

## Open Weather Map-Ajax Assignment
Open Weather Map provides weather information for locations around the world.  It is possible to find weather information using the name of a city, a geographic location (latitude and longitude), a zip code, or a special city id.  Open Weather Map provides an API (Application Programming Interface) for retrieving weather data as a JSON object by specifying the location in any of the above ways.  

Since we already have a simple application that retrieves "fake" weather information based upon zip code, we can modify it to work with real data by using the Open Weather Map API for zip codes.  

The first step is to go to [https://openweathermap.org/api](https://openweathermap.org/api) and sign up for a free account.  
When you sign up for an account you will be given an api key.  This key is important and it is needed to access the weather API.

The API call to use a zip code is: 
```
https://api.openweathermap.org/data/2.5/weather?us&APPID=your_api_key&units=imperial&zip=a_zip_code,us
```
Weather data is returned as a JSON object.  The following is the JSON object returned for a call for zip code 65803.
```
{
  "coord":{"lon":-93.3,"lat":37.26}, 
  "weather":
     [ {
         "id":800,
          "main":"Clear",
          "description":"clear sky",
          "icon":"01d"
       } ], 
   "base":"stations", 
   "main": 
      {
        "temp":64.4, 
        "pressure":1019, 
        "humidity":36, 
        "temp_min":64.4, 
        "temp_max":64.4 
       }, 
    "visibility":16093, 
    "wind":{"speed":9.17,"deg":330}, 
    "clouds":{"all":1}, 
    "dt":1540759920, 
    "sys":{ 
            "type":1,
            "id":1661,
            "message":0.0041,
            "country":"US",
            "sunrise":1540730075,
            "sunset":1540768719 
          }, 
    "id":420021920, 
    "name":"Springfield", 
    "cod":200 
 }
 ```

Modify the code of the zip code example to retrieve weather information for a zip code and display the current weather in a bulleted list.  Display the current temperature, the pressure in hPa, the humidity in %, the % of cloud cover, and the name of the city.

Link to the API Reference
[https://openweathermap.org/current#one](https://openweathermap.org/current#one)

Services like the one the assignment uses are known as Web Services and the API is an example of a RESTful API.

When you have your system working, push the code to your GitHub classroom for the assignment.