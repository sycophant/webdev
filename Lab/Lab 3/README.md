# Lab3-HTML-Containers
Initial repo for App Dev 1, Lab 3.

## Note!!!
I did not create an images directory because I am not providing an starter images.  Remember to create an images folder in your cloned repo.  Place all the images for your project in the new folder.  When you stage the image files, commit and push back to your remote GitHub repo, the images directory will be created in the remote repo.
