function calcWordFrequencies() {
	var words = prompt("Enter the string.");
	var str = words.split(' ');
	var freqmap = {};
	str.forEach(function(w){
		if(w in freqmap)
			freqmap[w] +=1;
		else
			freqmap[w] = 1;
	});
	for (i of str){
		console.log(i+" "+freqmap[i]);
	}
}