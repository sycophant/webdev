window.onload = function() {
    document.getElementById('search').addEventListener('click', function() {

        var zipcode = document.getElementById('zip').value;
        var xhr = new XMLHttpRequest(); 

        xhr.onload = function() {
            if (xhr.status !== 200) return console.log("City not Found!");

            var main = xhr.response.main;
            var clouds = xhr.response.clouds;
            var name = xhr.response.name;

            var html = "";
            html += "<h1>Forecast</h1>";
            html += "<ol>";
            //temperature, the pressure in hPa, the humidity in %, the % of cloud cover, and the name of the city
            html += "<li>temperature is " + main.temp + "</li>";
            html += "<li>pressure is " + main.pressure + " hPa" + "</li>";
            html += "<li>humidity is " + main.humidity + " %" + "</li>";
            html += "<li>cloud cover is " + clouds.all + " %" + "</li>";
            html += "<li>city is " + name + "</li>";
            html += "</ol>";

          // put the data in the document
          document.getElementById('forecast').innerHTML = html;
        }; //);

        xhr.open("GET", "https://api.openweathermap.org/data/2.5/weather?us&APPID=057a79310b77f82788209eae38715fcf&units=imperial&zip=" + zipcode + ",us");
        xhr.responseType = 'json';

        // 5) send the response
        xhr.send();
    });
    
    document.getElementById("zip").addEventListener("change", function(){
        // remove the previous forecast
        document.getElementById("forecast").innerHTML = "";
    });
}
                                                 