function ConvertCtoF(degreesCelsius){
    var Fah = (degreesCelsius * (9/5)) + 32;
    return Fah;
}

function ConvertFtoC(degreesFahrenheit){
    var Cel = (degreesFahrenheit - 32) * (5 / 9);
    return Cel;
}

function bodyLoaded() {
    var button = document.getElementById('ConvertButton');
    var C = document.getElementById('CInput');
    var F = document.getElementById('FInput');
    var err = document.getElementById('ErrDiv');
    var weather = document.getElementById('WeatherImage');
    button.addEventListener('click', function() {
        if (C.value != "") {
                if (isNaN(parseFloat(C.value))) {
                    err.innerHTML = C.value + " is not a number";
                } else {
                    F.value = ConvertCtoF(parseFloat(C.value));
                    err.innerText = "";
                }
		}else if (F.value != "") {
        	if (isNaN(parseFloat(F.value))) {
				err.innerText = F.value + " is not a number";
			} else {
				C.value = ConvertFtoC(parseFloat(F.value));
				err.innerText = "";
			}
		}
		if(F.value < 32){
			weather.src = "cold.gif";
		}else if (F.value <= 50){
			weather.src = "cool.gif";
		}else {
			weather.src = "warm.gif";
		}
    })
    C.addEventListener('input', function(){
    	if (F.value != ""){
    		F.value = "";
    	}
    });
    F.addEventListener('input', function(){
    	if (C.value != ""){
    		C.value = "";
    	}
    } );
};