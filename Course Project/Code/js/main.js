//Makes only debit or credit checkbox stay selected
$(':checkbox').click(function() {
	$(':checkbox').not(this).prop('checked', false);
});

//Functionality for the navigation bar.
function navBar($){
    "use strict";
    var  mn = $(".main-nav");
    mns = "main-nav-scrolled";
    hdr = $('header').height();
	$(window).scroll(function() {
		if( $(this).scrollTop() > hdr ) {
			mn.addClass(mns);
		} else {
			mn.removeClass(mns);
		}
	});
};

//handles building the accounts table. Takes a JSON, appends relevant data and updates table.
function pettyCash(jsonOBJ){

	//petty cash appending, ddepends on credit-debit check.
	if (jsonOBJ['type'] == 'credit'){
		$('#cd').before('<div class="row"><div class="cell" id="XXX">'+jsonOBJ['name']+'</div><div class="cell" id="XXX"></div><div class="cell tcredit" id="XXX">'+jsonOBJ['value']+'</div><div class="cell" id="XXX"></div>');
	};
	if (jsonOBJ['type'] == 'debit'){
		$('#cd').before('<div class="row"><div class="cell" id="XXX">'+jsonOBJ['name']+'</div><div class="cell tdebit" id="XXX">'+jsonOBJ['value']+'</div><div class="cell" id="XXX"></div><div class="cell" id="XXX"></div>');
	};

	//petty cash calculation, maps existing values in table for calculation.
	var debit = $(".tdebit").map(function() {return this.innerHTML;}).get();
	var credit = $(".tcredit").map(function() {return this.innerHTML;}).get();
	var balance = 0;

	//addition of debit and credit values.
	$.each(debit,function(){balance += parseFloat(this) || 0;});
	$.each(credit,function(){balance -= parseFloat(this) || 0;});

	//Offset to be stored in a row called c/d, and carried onto other parts.
	if (balance >= 0) {
		$('.cdval').html(parseInt(balance));
	}else {$('.cdval').html('('+parseInt(balance)+')');}
}

//functionality to parse the data as soon as it is entered, losing need to refresh the page.
$('.btn-unique').click(function(){

	//declaration of JSON object to which relevant data is parsed.
	var jsonOBJ = {};

	if ($('#debit').is(':checked')){
		jsonOBJ['type'] = 'debit';
	}else {jsonOBJ['type'] = 'credit';};
	jsonOBJ['name'] = $('#forma').val();
	jsonOBJ['value'] = $('#formc').val();
	//calling of builder function on the parsed JSON.
	pettyCash(jsonOBJ);;
});

//building of table from previously entered data that exists in the database.
$(document).ready(function() {

	var taccounts = null;

	//ajax call to get data from mongoDB.
	$.ajax({
		'async': false,
		'type': "GET",
		'global': false,
		'dataType': 'json',
		'url': "/get",
		'data': { 'request': "", 'target': 'arrange_url', 'method': 'method_target' },
		'success': function (data) {
			taccounts = data;
		}
	});

	//for loop to go through all the documents in the collection.
	for (var i = 0; i<Object.keys(taccounts).length; i++){
		pettyCash(taccounts[i]);
	};
});