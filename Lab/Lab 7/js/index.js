/********************************************************
 * Javascript object assignment.  Complete the following 
 * code based upon our discussion in class and the 
 * material presented in section 8.2.
 ********************************************************/

function Student(sId = "", lname = "", fname="", mail="") {
    var id = 0;
    var lastName = '';
    var firstName = '';
    var email = '';
    //getters
	this.getSID = function(){
		return this.id;
	};
	this.getSFName = function(){
		return this.firstName;
	};
	this.getSLName = function(){
		return this.lastName;
	};
	this.geteMail = function(){
		return this.email;
	};
	//setters
	this.setSID = function(sId){
		this.id = sId;
	};
	this.setSFName = function(fname){
		this.firstName = fname;
	};
	this.setSLName = function(lname){
		this.lastName = lname;
	};
	this.seteMail = function(mail){
		this.email = mail;
	};
};

Student.prototype.getFullName = function() {
	let _this = this;
	let fullName = _this.SFName + " " + _this.SLName;
	return fullName;
};


function App() { 
	var roster = [];
	this.getRoster = function(){
		return this.roster;
	};
	this.setRoster = function(rost){
		this.roster = rost;
	};
};

// async function getFile(){
// 	var response = await fetch('http://mcs.drury.edu/ssigman/DUCS_EMS/getAllStudents.php')
// 	var tableJSON = await response.json();
// 	console.log('table acquired');
// 	return tableJSON;
// }

App.prototype.initialize = function(){
	var _this = this;
	var xhr = new XMLHttpRequest();
	xhr.onload = function(){
		var tableJSON = xhr.response;
		console.log(tableJSON);
		var rost = [];
		for (var i = 0; i<tableJSON.length; i++){
			let stu = new Student();
			stu.setSID(tableJSON[i].studentID);
			stu.setSFName(tableJSON[i].firstName);
			stu.setSLName(tableJSON[i].lastName);
			stu.seteMail(tableJSON[i].email);
			rost.push(stu);
			console.log('parsing');
		};
		_this.setRoster(rost);
		_this.addRoster();
	};
	xhr.open("GET", "http://mcs.drury.edu/ssigman/DUCS_EMS/getAllStudents.php");
	xhr.responseType = "json";
	xhr.send();
	console.log('parsed');
};

App.prototype.addRoster = function(){
	var classRoster = this.getRoster();
	var html = '';
	for (var i = 0; i < classRoster.length; i++){
		var row = '<tr>' + 
			'<td>' + classRoster[i].getSID() + '<td/>' +
			'<td>' + classRoster[i].getSFName() + '<td/>' +
			'<td>' + classRoster[i].getSLName() + '<td/>' +
			'<td>' + classRoster[i].geteMail() + '<td/>' +
			'</tr>';
			console.log('added');
		html += row;}
	document.getElementById('rosterTable').innerHTML = html;
	console.log('inserted');
};

window.onload = function() {
	console.log('start');
	var myApp = new App();
	console.log('create');
	myApp.initialize();
	console.log('init');
};