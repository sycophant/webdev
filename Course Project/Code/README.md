Functional accounting spreadsheet.

Has placeholders for balance sheet, petty cash account and ratios. Balance sheet and ratios account populated with dummy data.

Petty cash account has capacity to add any entry of any amount (can be unnamed and/or have no value) as long as it is specified if it is debit or credit. Using all the entries, the balance c/d (carry down) is calculated which will be used in other parts of the spreadsheet.

Uses jquery and bootstrap for frontend, nodejs and mongodb (with mongoose) for backend.