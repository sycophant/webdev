# Javascript-Objects-Assignment
Use the attached index.html file as the starting point for your code.  You need to:

Define a Student class in Javascript with has id, lastName, firstName, and email properties.
Use an AJAX call to read the array of all students from the Web Service.
Create an array, named roster, which stores each student from the JSON as an instance of your Student class.  (ie. roster[0] should be an instance of the Student class with the data for the first student in the JSON (Daffy Duck).
Add one row to the table for each Student object in the roster array.
Format your table so that rows alternate colors.
Create an app object that is the controller for the application.  The app object needs an initialize method and a addRosterTable method.