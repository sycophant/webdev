// 1) How many direct child elements does the article element have?
function getArticles() {
    var articlesNDoc = document.getElementsByTagName('article');
    console.log("No. of articles: "+articlesNDoc.length);
}

// 2) Get the answer for 1 using the querySelectorAll call.
function getArticlesByQuery() {
   var articlesNDoc = document.querySelectorAll('article');
   console.log("No. of articles: "+articlesNDoc.length);
}

// 3) Get the number of div elements in the document.
function getDivCount() {
  var numDiv = document.getElementsByTagName('div');
  console.log("Number of divs: "+numDiv.length);
}

// 4) Get the number of divs in the article element using querySelectorAll.
function getDivsInArticle() {
   var numDivArticle = document.querySelectorAll('article>div');
   console.log("Num of divs in article: "+numDivArticle.length);
}

// 5) Print the children of Article.
function getChildrenOfArticle() {
    // var articleNode = document.getElementsByTagName('article');
    // var numChildArticle = articleNode[0].children;
    
}

// 6) Write a function that finds all the sibilings of a given element.
//    The selector parameter is used to "select" the node whose sibilings
//    are reported.
function findSibilings(selector) {
    
}