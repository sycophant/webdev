// This file contains the definitions of the object literals a
// and the functions you need to complete this assignment.  
// Start by completing the code for the red_die and the green_die 
// objects give the specification below.
// A die object has two properties. Defined as:
//     color - a string that specifies the color, either 'red' or 'green'.
//     faces - an array of six integers with the values 1, 2, ... , 6.
// In addition an object has a method named roll(), which returns the 
// value of a randomly determined face.  The code to randomly determine
// a face value (ie. to choose the result of a roll) is
//
//    var randNum = Math.floor((Math.random() * this.faces.length) + 1);
//    return this.faces[randNum-1];
//
// Note:  This code requires that your faces propertie be named faces.  The 
//        this keyword is a reference to the object.

/*
   red_die a die object.  The object has a color of red and faces containg
   one of the following numerals: 1, 2, 3, 4, 5, 6.  In addition the class has
   a method roll(), which randomly returns one of the six face values.
 */

var red_die = {
    color: "red",
    faces: [1, 2, 3, 4, 5, 6],
    roll_die: roll_die
}

var green_die = {
    color: "green",
    faces: [1, 2, 3, 4, 5, 6],
    roll_die: roll_die
}

function roll_die(die){
    var randNum = Math.floor(Math.random() * this.faces.length + 1);
    return (this.faces[randNum-1]);
};

/*
  play_game simulates 10 tosses of the die and reports
  the results on the console.  The results are logged to 
  the console and are inserted in the results_panel (the 
  div with id="results_panel") on the game page.
 */
function play_game(red_die, green_die) {
    // log game header
    console.log('----- Game -----');

    // find the result panel
    var resultPanel = document.getElementById("results_panel");

    // display game header in the result panel on the page
    resultPanel.innerHTML = resultPanel.innerHTML +
        '<p class="result_p">' + '----- Game -----' +
        '</p>';

    // play 10 turns
    for (var i = 1; i <= 10; i++) {
        // roll both die to determine thier value
        var die1_value = red_die.roll_die();
        var die2_value = green_die.roll_die();
        // determine whether there was a winner or 
        // whether a tie resulted
        var tie_flag = Boolean(die1_value == die2_value);

        var winner; // the winning die
        var winner_roll; // the value the winner rolled
        var loser; // the losing die
        var loser_roll; // the value the loser rolled

        // check to see if there was a tie
        if (!tie_flag) {
            // no tie - determine tbe winner
            if (die1_value > die2_value) {
                winner = red_die;
                loser = green_die;
                winner_roll = die1_value;
                loser_roll = die2_value;
            } else {
                winner = green_die;
                loser = red_die;
                winner_roll = die2_value;
                loser_roll = die1_value;
            };

            // log the winner
            console.log("Roll: " + i + " Winner: " + winner.color + " => " + winner_roll + " Loser: " + loser.color + " => " + loser_roll + ".");

            // add throw summary to the page
            resultPanel.innerHTML = resultPanel.innerHTML +
                '<p class="result_p">' +
                'Roll: ' + i + ' Winner: ' + winner.color + ' => ' + winner_roll + ' Loser: ' + loser.color + ' => ' + loser_roll + '.' +
                '</p>';
        } else {
            // there was a tie
            console.log("Roll " + i + " was a draw.")

            // add tie notification to page
            resultPanel.innerHTML = resultPanel.innerHTML +
                '<p class="result_p">' +
                "Roll " + i + " was a draw." +
                '</p>';
        };
    };
}