//importing of packages needed for server to function
var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require('body-parser');

//binding database to a port
mongoose.connect("mongodb://localhost:27017/taccount");

//setting up template by which data will be stored in collection
var taccount = new mongoose.Schema({
    type: { type: String, required:true },
    name: { type: String },
    value:   { type: Number, min: 0, max: 999999999999 },
});

//declaration of objects from imported package and template
var tacc = mongoose.model("tacc", taccount);
var app = express();

app.use('/', express.static(__dirname + '/'));
app.use(express.json());    //holdover from ajax call to send post data, crashes mongoDB.
app.use(bodyParser.urlencoded({ extended: false }));

//sets up path to get collection in database
app.get("/get", (req, res) => {
    tacc.find({}, (err, taccs)=>{
        res.json(taccs);
    });
});
//gets data from form and appends it to database
app.post("/create", function(req, res) {
    //creates an instance of the model built out of the template
    var entry = new tacc({
        type: req.body.type,
        name: req.body.name,
        value: req.body.value,
    });
    //call to save data and confirm appending.
    entry.save(function(err, entry) {
        if (err) {res.status(400).send(err);
        } else {console.log("Entry was saved.");}
    });
});

app.listen(3000);
