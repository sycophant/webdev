var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: false}));

//example server get
app.get("/hello", function(req, res){
	var name = req.query.name;
	var hobby = req.query.hobby;

	res.send("<h1>Hello, "+name+"</h1>"+"Your hobby is "+hobby+".");
});

app.post("/hello", function(req, res){
	var name = req.body.name;
	var hobby = req.body.hobby;

	res.send("<h1>Hello, "+name+"</h1>"+"Your hobby is "+hobby+".");
});

app.listen(3000);